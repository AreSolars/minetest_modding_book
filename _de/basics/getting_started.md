---
title: Erste Schritte
layout: default
root: ../..
idx: 1.1
description: Lerne wie man einen Mod-Ordner mit init.lua, mod.conf und mehr anlegt.
redirect_from:
- /de/chapters/folders.html
- /de/basics/folders.html
---

## Einführung <!-- omit in toc -->

Es ist wesentlich, den Aufbau der grundlegenden Strukturen des Mod-Verzeichnisses zu verstehen, wenn man Mods erstellt.

- [Was sind Spiele und Mods?](#was-sind-spiele-und-mods)
- [Wo werden die Mods gespeichert?](#wo-werden-die-mods-gespeichert)
- [Das Mod-Verzeichnis](#das-mod-verzeichnis)
- [mod.conf](#modconf)
	- [Abhängigkeiten](#abhängigkeiten)
- [Mod Packs](#mod-packs)
- [Beispiel](#beispiel)
	- [Mod-Verzeichnis](#mod-verzeichnis)
	- [init.lua](#initlua)
	- [mod.conf](#modconf-1)


## Was sind Spiele und Mods?

Die Stärke von Minetest ist die Fähigkeit, Spiele zu erstellen, ohne eigene Voxel-Grafik, Voxel Algorithmen und raffinierten Netzwerk-Code erstellen zu müssen.

In Minetest ist ein Spiel eine Sammlung von Modulen, welche miteinander arbeiten, um den Inhalt und das Verhalten des Spiels zur Verfügung zu stellen.
Ein Modul, allgemein als Mod bezeichnet, ist eine Sammlung von Skripten und Ressourcen.
Es ist möglich, ein Spiel mit nur einem Mod zu erstellen, aber das wird nur selten gemacht, weil es sonst nicht mehr so einfach ist, Teile des Spieles unabhängig von den Anderen anzupassen oder zu ersetzen.

Ebenfalls ist es möglich, Mods außerhalb eines Spieles zu verbreiten. In diesem Fall sind sie ebenfalls *Mods*, aber in einem traditionellerem Sinn: *Modifikationen*. Diese Mods verändern oder erweitern die Eigenschaften eines Spiels.

Sowohl die Mods die im Spiel enthalten sind, als auch die Mods von Dritten nutzen die selbe API (Programmierschnittstelle).


## Wo werden die Mods gespeichert?

<a name="mod-locations"></a>

Jede Mod hat ihr eigenes Verzeichnis, wo sich ihre Lua-Quelltexte, Texturen, Modelle und Tondateien befinden. Minetest überprüft verschiedene Orte auf Mods. Diese Orte werden allgemein *Mod-Lade-Verzeichnisse* genannt.

