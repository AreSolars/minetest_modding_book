---
title: Blöcke, Ttems und Craften
layout: default
root: ../..
idx: 2.1
description: Benutze register_node, register_iem und register_craft um zu lernen wie man Blöcke, Items und Rezepte erstellen.
redirect_from: /de/chapters/nodes_items_crafting.html
---

## Einführung <!-- omit in toc -->

Neue Blöcke, Craftitems und Rezepte zu erstellen, sind Grundlagen von vielen Mods

- [Was sind Blöcke und Craftitems?](#was-sind-blöcke-und-craftitems)
- [Items erstellen](#items-erstellen)
    - [Itemnamen](#itemnamen)
    - [Itemaliase](#itemaliase)
## Was sind Blöcke und Craftitems?

Blöcke, Craftitems und tools sind alles Items. Ein Items ist etwas das im Inventar gefunden werden kann - Sogar wenn es nicht im normalen gameplay nicht möglich ist.

Ein Block ist ein Item das plaziert werden kann oder in der Welt gefunden werden kann. Jede Position muss belegt werden mit ein und nur einen Block - Scheinbare leere Position sind normalerweise Luftblöcke.

Ein Craftitem kann nicht plazier werden man kann es nur im Inventar finden oder als gedropptes Item in der Welt.

Ein Werkzeug hat die Fähigkeit sich abzunutzen und hat normalerweise nicht standardmäßige Abbaufähigkeiten. In Zukunft we Craftitems und Werkzeuge wahrscheinlich verschmelzen weil die Unterscheidung zwinschen ihnen eher ausgedacht ist.

## Items erstellen

Item Definitionen bestehen aus einen *Itemnamen* und einer *Definitions Tabelle*. Die Definitions Tabelle beinhaltet Attribute welche das Verhalten eines Items beinflussen.

```lua
minetest.register_craftitem("modname:itemname", {
    description = "Mein spezielles Item",
    inventory_image = "modname_itemname.png"
})
```

### Itemnamen

jedes Item hat ein Itemnamen welches auf sich verweist, es sollte folgendes Format haben:

    modname:itemname

### Itemaliase
Items können auch *Aliase* haben die auf ihren Namen zeigen. Ein *Alias* ist ein nachgemachter Item Name der dazu führt, dass die Egine alle Aliase so behandelt als wären es Itemnamen. Da sind zwei verbreitete Varianten um das zu nutzen:

* Umbenannte entfernte Items in etwas anderes umzubenennen. Es kann Unbekannte Items in der Welt oder im Inventar geben, wenn ein Gegenstand ohne Korrektur aus einen Mod entfernt wird.
* Ein Abkürzung hinzufügen. `/giveme dirt` ist einfacher als `/giveme default:dirt`.

Ein Itemalias zu erstellen ist richtig einfach. Ein guter Weg um sich die Reinfolge von der Argumenten zu merken ist `von → zu` wo *von* der alias ist und *zu* das Orginal.

```lua
minetest.register_alias("dirt", "default:dirt")
```

Mods müssen sicher gehen, dass Alias aufgelöst werden, bevor sie sich direkt mit Itemnamen befassen, da die Engine dies nicht tut. Das ist allerdings ziemlich einfach:

```lua
itemname = minetest.registered_aliases[itemname] or itemname
```
